Feature:EC Process
    tests:
    1. users can checkout successfully
    2. amount of money on order summary is correct
    Scenario: BQtw TW EC Order Process - Member Checkout
        Given [ZH-TW Member]Go to Product Page
        When [ZH-TW Member]Go to EC Product Page
        When [ZH-TW Member]Cart Page
        And [ZH-TW Member]Login Page
        And [ZH-TW Member]Shipping Page
        Then [ZH-TW Member]Payment Page
        Given [ZH-TW Member]Cart Page - Remove Item
    Scenario: BQE EU-Italy EC Order Process - Guest Checkout
        Given [EN-EU Guest]Go to Product Page
        When [EN-EU Guest]Cart Page
        And [EN-EU Guest]Shipping Page
        Then [EN-EU Guest]Payment Page
    Scenario: BQE EU-Italy EC Order Process - Member Checkout
        Given [EN-EU Member]Go to Product Page
        When [EN-EU Member]Cart Page
        And [EN-EU Member]Login Page
        And [EN-EU Member]Shipping Page
        Then [EN-EU Member]Payment Page
        Given [EN-EU Member]Cart Page - Remove Item



    
        
