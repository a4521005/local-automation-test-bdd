//npm run test
const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const { hidden } = require('kleur')
const puppeteer = require('puppeteer')
const cicGA="?utm_source=autotest&utm_medium=CIC"

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        headless:false,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

Given("[EN-EU Guest]Go to Product Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://shop.benq.eu/eu-buy/ex3501r.html'+cicGA)
    const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const buyNow = '#product-addtocart-button'
    await this.page.waitForSelector(buyNow)
    await this.page.click(buyNow)
    await this.page.waitForTimeout(20000)//等待20000毫秒
})

When("[EN-EU Guest]Cart Page",{timeout: 36 * 5000},async function(){
    //Cart Page
    const cartPageUrl = await this.page.url()
    console.log('Cart Page URL:',cartPageUrl)
    expect(cartPageUrl).to.include('checkout/cart')//斷言:此page的url必須包含example.com
    const addItems = '#shopping-cart-table > tbody > tr > td.col.qty > div > div > div > button.increaseQty-cart-item'
    await this.page.waitForSelector(addItems)
    await this.page.click(addItems)//增加商品
    await this.page.waitForTimeout(5000)
    const qtyUpdate = '#shopping-cart-table > tbody > tr > td.col.qty > div > div > span.input-text-qty'
    await this.page.waitForSelector(qtyUpdate)//qty更新
    var qty = await this.page.$eval(qtyUpdate,element=>element.innerHTML)
    expect(qty).to.be.a('string','2')
    // await this.page.waitForTimeout(5000)
    // const needHelp = 'div.embeddedServiceInvitation'
    // await this.page.waitForSelector(needHelp, {visible:true})
    // const acceptNeedHelp = 'div.embeddedServiceInvitationFooter > button:nth-child(2)'
    // await this.page.waitForSelector(acceptNeedHelp, {visible:true})
    // await this.page.click(acceptNeedHelp)
    // //div.embeddedServiceInvitation
    await this.page.waitForTimeout(5000)
    //'#maincontent > div.columns > div > div.cart-summary > ul > li:nth-child(3) > button'
    const guestCheckoutButton = 'div.cart-summary > ul > li:nth-child(3) > button'
    await this.page.waitForSelector(guestCheckoutButton)//Guest Checkout
    await this.page.click(guestCheckoutButton)//Guest Checkout
    await this.page.waitForTimeout(20000)//等待20000毫秒
})

When("[EN-EU Guest]Shipping Page",{timeout: 36 * 5000},async function(){
    await this.page.waitForTimeout(5000)//等待10000毫秒
    await this.page.waitForSelector('#shipping')
    const shippingPageUrl = await this.page.url()
    console.log('Shipping Page URL:',shippingPageUrl)
    expect(shippingPageUrl).to.include('/checkout/')//斷言:此page的url必須包含example.com
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.type('#customer-email','a4521005@outlook.com',{delay:10})//在email欄位輸入非會員email
    await this.page.waitForTimeout(5000)//等待100毫秒
    await this.page.type('#shipping-new-address-form > div:nth-child(1) > div > input','Celine',{delay:10})//在first name欄位輸入收件人first name
    await this.page.waitForTimeout(100)//等待100毫秒
    await this.page.type('#shipping-new-address-form > div:nth-child(2) > div > input','Chiu',{delay:10})//在last name欄位輸入收件人last name
    await this.page.waitForTimeout(100)//等待100毫秒
    const selectItaly = await this.page.$('select[name="country_id"]');
    await selectItaly.type('Italy');
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.type('#shipping-new-address-form > fieldset > div > div > div > input','address',{delay:10})//在street address欄位輸入address
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.keyboard.press('Enter')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.waitForSelector('#shipping-new-address-form > div:nth-child(8) > div > input')
    await this.page.type('#shipping-new-address-form > div:nth-child(8) > div > input','Eichgraben',{delay:10})//在city欄位輸入city
    await this.page.waitForTimeout(8000)//等待8000毫秒
    // const selectBurgenland = await this.page.$('select[name="region_id"]');
    // await selectBurgenland.type('Niederösterreich');
    // await this.page.waitForTimeout(9000)//等待9000毫秒
    await this.page.type('#shipping-new-address-form > div:nth-child(11) > div > input','01100',{delay:10})//在zip code欄位輸入zip code
    await this.page.waitForTimeout(5000)//等待5000毫秒//歐洲會辨認郵遞區號所以要等久一點
    await this.page.type('#shipping-new-address-form > div:nth-child(12) > div > input','886929861005',{delay:10})//在Phone Number欄位輸入Phone Number
    await this.page.waitForTimeout(5000)//等待5000毫秒
    //取得price, shipping, tax的文字
    const shippingCartSubtotalTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span'
    await this.page.waitForSelector(shippingCartSubtotalTextElement)
    const shippingCartSubtotalText = await this.page.$eval(shippingCartSubtotalTextElement,element=>element.innerHTML)
    //const shippingCartSubtotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span')
    const shippingShippingTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span'
    await this.page.waitForSelector(shippingShippingTextElement)
    const shippingShippingText = await this.page.$eval(shippingShippingTextElement,element=>element.innerHTML)
    //const shippingShippingText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span')
    const shippingTaxTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span'
    await this.page.waitForSelector(shippingTaxTextElement)
    const shippingTaxText = await this.page.$eval(shippingTaxTextElement,element=>element.innerHTML)
    //const shippingTaxText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span')
    const shippingOrderTotalElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span'
    await this.page.waitForSelector(shippingOrderTotalElement)
    const shippingOrderTotalText = await this.page.$eval(shippingOrderTotalElement,element=>element.innerHTML)
    //const shippingOrderTotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span')
        
    //去除EUR 
    const shippingCartSubtotalNumIncludeComma = shippingCartSubtotalText.replace("EUR ","")
    const shippingShippingOnlyNumIncludeComma = shippingShippingText.replace("EUR ","")
    const shippingTaxOnlyNumIncludeComma = shippingTaxText.replace("EUR ","")
    const shippingOrderTotalNumIncludeComma = shippingOrderTotalText.replace("EUR ","")
    //去除逗號 
    function clear(str) { 
        str = str.replace(/,/g, "");//取消字串中出現的所有逗號 
        return str; 
    } 
    const shippingCartSubtotalNum = clear(shippingCartSubtotalNumIncludeComma)
    const shippingShippingOnlyNum = clear(shippingShippingOnlyNumIncludeComma)
    const shippingTaxOnlyNum = clear(shippingTaxOnlyNumIncludeComma)
    const shippingOrderTotalNum = clear(shippingOrderTotalNumIncludeComma)
    //將字串轉換成數字(包含小數值)
    const shippingCartSubtotal = parseFloat(shippingCartSubtotalNum)
    const shippingShipping = parseFloat(shippingShippingOnlyNum)
    const shippingTax = parseFloat(shippingTaxOnlyNum)
    const shippingOrderTotal = parseFloat(shippingOrderTotalNum )
    console.log('cartSubtotal(Shipping Page):',shippingCartSubtotal)
    console.log('shipping(Shipping Page):',shippingShipping)
    console.log('tax(Shipping Page):',shippingTax )
    console.log('orderTotal(Shipping Page):',shippingOrderTotal)
    //確認total information上的金額
    const shippingOrderTotalActual = shippingCartSubtotal+shippingShipping+shippingTax
    const shippingTaxRate = 0.22 //義大利稅率
    const shippingCartSubtotalAndShipping = shippingCartSubtotal+shippingShipping
    const shippingTaxActualUnfixed = shippingCartSubtotalAndShipping*shippingTaxRate
    const shippingTaxActualFixed = shippingTaxActualUnfixed.toFixed(2)//四捨五入取到小數點第二位
    const shippingTaxActual= parseFloat(shippingTaxActualFixed)//因為toFixed(2)回傳的是字串
    console.log('totalActual(Shipping Page):',shippingOrderTotalActual)//實際計算(應得)的數字
    console.log('taxActual(Shipping Page):',shippingTaxActual)//實際計算(應得)的數字
    //確認total information和實際計算(應得)的數字是否相同
    expect(shippingOrderTotal).to.equal(shippingOrderTotalActual)
    expect(shippingTax).to.equal(shippingTaxActual)
    await this.page.click('#next-step-trigger')//Next
    await this.page.waitForSelector('#next-step-trigger',{
        hidden:true, 
        timeout:10000
    })//等待element隱藏的時間是否在10000毫秒內
    //await shouldNotExist(page,'#next-step-trigger')//Next
})

Then("[EN-EU Member]Payment Page",{timeout: 12 * 5000},async function(){
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.waitForSelector('#co-payment-form > fieldset > legend > span')
    await this.page.waitForSelector('#co-payment-form > fieldset > div.shipping-information')
    const paymentPageUrl = await this.page.url()
    console.log('Payment Page URL:',paymentPageUrl)
    expect(paymentPageUrl).to.include('/checkout/#payment')//斷言:此page的url必須包含example.com
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.waitForSelector('#opc-sidebar > div.opc-block-summary')
    //取得price, shipping, tax的文字
    const paymentCartSubtotalTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span'
    await this.page.waitForSelector(paymentCartSubtotalTextElement)
    const paymentCartSubtotalText = await this.page.$eval(paymentCartSubtotalTextElement,element=>element.innerHTML)
    //const paymentCartSubtotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span')
    const paymentShippingTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span'
    await this.page.waitForSelector(paymentShippingTextElement)
    const paymentShippingText = await this.page.$eval(paymentShippingTextElement,element=>element.innerHTML)    
    //const paymentShippingText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span')
    const paymentTaxTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span'
    await this.page.waitForSelector(paymentTaxTextElement)
    const paymentTaxText = await this.page.$eval(paymentTaxTextElement,element=>element.innerHTML)    
    //const paymentTaxText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span')
    const paymentOrderTotalTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span'
    await this.page.waitForSelector(paymentOrderTotalTextElement)
    const paymentOrderTotalText = await this.page.$eval(paymentOrderTotalTextElement,element=>element.innerHTML)
    //const paymentOrderTotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span')
    //去除EUR 
    const paymentCartSubtotalNumIncludeComma = paymentCartSubtotalText.replace("EUR ","")
    const paymentShippingOnlyNumIncludeComma = paymentShippingText.replace("EUR ","")
    const paymentTaxOnlyNumIncludeComma = paymentTaxText.replace("EUR ","")
    const paymentOrderTotalNumIncludeComma = paymentOrderTotalText.replace("EUR ","")
    //去除逗號 
    function clear(str) { 
        str = str.replace(/,/g, "");//取消字串中出現的所有逗號 
        return str; 
    } 
    const paymentCartSubtotalNum = clear(paymentCartSubtotalNumIncludeComma)
    const paymentShippingOnlyNum = clear(paymentShippingOnlyNumIncludeComma)
    const paymentTaxOnlyNum = clear(paymentTaxOnlyNumIncludeComma)
    const paymentOrderTotalNum = clear(paymentOrderTotalNumIncludeComma)
    //將字串轉換成數字
    const paymentCartSubtotal = parseFloat(paymentCartSubtotalNum)
    const paymentShipping = parseFloat(paymentShippingOnlyNum)
    const paymentTax = parseFloat(paymentTaxOnlyNum )
    const paymentOrderTotal = parseFloat(paymentOrderTotalNum )
    console.log('cartSubtotal(Payment Page):',paymentCartSubtotal)
    console.log('shipping(Payment Page):',paymentShipping)
    console.log('tax(Payment Page):',paymentTax )
    console.log('orderTotal(Payment Page):',paymentOrderTotal)
    //確認total information上的金額
    const paymentOrderTotalActual = paymentCartSubtotal+paymentShipping+paymentTax
    const paymentTaxRate = 0.22 //義大利稅率
    const paymentCartSubtotalAndShipping = paymentCartSubtotal+paymentShipping
    const paymentTaxActualUnfixed = paymentCartSubtotalAndShipping*paymentTaxRate
    const paymentTaxActualFixed = paymentTaxActualUnfixed.toFixed(2)//四捨五入取到小數點第二位
    const paymentTaxActual= parseFloat(paymentTaxActualFixed)//因為toFixed(2)回傳的是字串
    console.log('totalActual(Payment Page):',paymentOrderTotalActual)//實際計算(應得)的數字
    console.log('taxActual(Payment Page):',paymentTaxActual)//實際計算(應得)的數字
    //確認total information和實際計算(應得)的數字是否相同
    expect(paymentOrderTotal).to.equal(paymentOrderTotalActual)
    expect(paymentTax).to.equal(paymentTaxActual)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Given("[EN-EU Member]Go to Product Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://shop.benq.eu/eu-buy/ex3501r.html'+cicGA)
    const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const buyNow = '#product-addtocart-button'
    await this.page.waitForSelector(buyNow)
    await this.page.click(buyNow)
    await this.page.waitForTimeout(20000)//等待20000毫秒
})

When("[EN-EU Member]Cart Page",{timeout: 12 * 5000},async function(){
    //Cart Page
    const cartPageUrl = await this.page.url()
    console.log('Cart Page URL:',cartPageUrl)
    expect(cartPageUrl).to.include('checkout/cart')//斷言:此page的url必須包含example.com
    const addItems = '#shopping-cart-table > tbody > tr > td.col.qty > div > div > div > button.increaseQty-cart-item'
    await this.page.click(addItems)//增加商品
    await this.page.waitForTimeout(5000)
    const qtyUpdate = '#shopping-cart-table > tbody > tr > td.col.qty > div > div > span.input-text-qty'
    await this.page.waitForSelector(qtyUpdate)//qty更新
    var qty = await this.page.$eval(qtyUpdate,element=>element.innerHTML)
    expect(qty).to.be.a('string','2')
    // await this.page.waitForTimeout(5000)
    // const needHelp = 'div.embeddedServiceInvitation'
    // await this.page.waitForSelector(needHelp, {visible:true})
    // const acceptNeedHelp = 'div.embeddedServiceInvitationFooter > button:nth-child(2)'
    // await this.page.waitForSelector(acceptNeedHelp, {visible:true})
    // await this.page.click(acceptNeedHelp)
    //div.embeddedServiceInvitation
    await this.page.waitForTimeout(5000)
    //#maincontent > div.columns > div > div.cart-summary > ul > li:nth-child(1) > button
    const memberCheckoutButton = 'div.cart-summary > ul > li:nth-child(1) > button'
    await this.page.waitForSelector(memberCheckoutButton)//Member Checkout
    await this.page.click(memberCheckoutButton)//Member Checkout
    await this.page.waitForTimeout(20000)//等待20000毫秒
})

When("[EN-EU Member]Login Page",{timeout: 12 * 5000},async function(){
    const loginPageUrl = await this.page.url()
    console.log('Login Page URL:',loginPageUrl)
    expect(loginPageUrl).to.include('https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=Magento&function=Login&lang=en-eu')//斷言:此page的url必須包含example.com
    await this.page.waitForTimeout(1000)//等待1000毫秒
    await this.page.waitForSelector('body > div.block.log_in > div > div > div.col-sm-7.col-xs-12.log_left > div')
    await this.page.type('#userName','celinetest123@gmail.com',{delay:100})
    await this.page.type('#password','test1234',{delay:100})
    await this.page.click('#login')
    await this.page.waitForTimeout(30000)//等待30000毫秒
})

When("[EN-EU Member]Shipping Page",{timeout: 12 * 5000},async function(){
    const shippingPageUrl = await this.page.url()
    console.log('Shipping Page URL:',shippingPageUrl)
    expect(shippingPageUrl).to.include('/checkout/')//斷言:此page的url必須包含example.com
    await this.page.waitForSelector('#shipping')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const chooseAddress = '#checkout-step-shipping > div > div > div > div.shipping-address-item.not-selected-item > button.action.action-select-shipping-item'
    await this.page.waitForSelector(chooseAddress)
    await this.page.click(chooseAddress)//選擇地址簿
    await this.page.waitForSelector('#opc-sidebar > div.opc-block-summary')
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //取得price, shipping, tax的文字
    const shippingCartSubtotalTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span'
    await this.page.waitForSelector(shippingCartSubtotalTextElement)
    const shippingCartSubtotalText = await this.page.$eval(shippingCartSubtotalTextElement,element=>element.innerHTML)
    //const shippingCartSubtotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span')
    const shippingShippingTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span'
    await this.page.waitForSelector(shippingShippingTextElement)
    const shippingShippingText = await this.page.$eval(shippingShippingTextElement,element=>element.innerHTML)
    //const shippingShippingText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span')
    const shippingTaxTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span'
    await this.page.waitForSelector(shippingTaxTextElement)
    const shippingTaxText = await this.page.$eval(shippingTaxTextElement,element=>element.innerHTML)
    //const shippingTaxText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span')
    const shippingOrderTotalElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span'
    await this.page.waitForSelector(shippingOrderTotalElement)
    const shippingOrderTotalText = await this.page.$eval(shippingOrderTotalElement,element=>element.innerHTML)
    //const shippingOrderTotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span')
    //去除EUR 
    const shippingCartSubtotalNumIncludeComma = shippingCartSubtotalText.replace("EUR ","")
    const shippingShippingOnlyNumIncludeComma = shippingShippingText.replace("EUR ","")
    const shippingTaxOnlyNumIncludeComma = shippingTaxText.replace("EUR ","")
    const shippingOrderTotalNumIncludeComma = shippingOrderTotalText.replace("EUR ","")
    //去除逗號 
    function clear(str) { 
        str = str.replace(/,/g, "");//取消字串中出現的所有逗號 
        return str; 
    } 
    const shippingCartSubtotalNum = clear(shippingCartSubtotalNumIncludeComma)
    const shippingShippingOnlyNum = clear(shippingShippingOnlyNumIncludeComma)
    const shippingTaxOnlyNum = clear(shippingTaxOnlyNumIncludeComma)
    const shippingOrderTotalNum = clear(shippingOrderTotalNumIncludeComma)
    //將字串轉換成數字(包含小數值)
    const shippingCartSubtotal = parseFloat(shippingCartSubtotalNum)
    const shippingShipping = parseFloat(shippingShippingOnlyNum)
    const shippingTax = parseFloat(shippingTaxOnlyNum)
    const shippingOrderTotal = parseFloat(shippingOrderTotalNum )
    console.log('cartSubtotal(Shipping Page):',shippingCartSubtotal)
    console.log('shipping(Shipping Page):',shippingShipping)
    console.log('tax(Shipping Page):',shippingTax )
    console.log('orderTotal(Shipping Page):',shippingOrderTotal)
    //確認total information上的金額
    const shippingOrderTotalActual = shippingCartSubtotal+shippingShipping+shippingTax
    const shippingTaxRate = 0.22 //義大利稅率
    const shippingCartSubtotalAndShipping = shippingCartSubtotal+shippingShipping
    const shippingTaxActualUnfixed = shippingCartSubtotalAndShipping*shippingTaxRate
    const shippingTaxActualFixed = shippingTaxActualUnfixed.toFixed(2)//四捨五入取到小數點第二位
    const shippingTaxActual= parseFloat(shippingTaxActualFixed)//因為toFixed(2)回傳的是字串
    console.log('totalActual(Shipping Page):',shippingOrderTotalActual)//實際計算(應得)的數字
    console.log('taxActual(Shipping Page):',shippingTaxActual)//實際計算(應得)的數字
    //確認total information和實際計算(應得)的數字是否相同
    expect(shippingOrderTotal).to.equal(shippingOrderTotalActual)
    expect(shippingTax).to.equal(shippingTaxActual)
    await this.page.click('#next-step-trigger')//Next
    await this.page.waitForSelector('#next-step-trigger',{
        hidden:true, 
        timeout:10000
    })//等待element隱藏的時間是否在10000毫秒內
    //await shouldNotExist(page,'#next-step-trigger')//Next
})
Then("[EN-EU Guest]Payment Page",{timeout: 12 * 5000},async function(){
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.waitForSelector('#co-payment-form > fieldset > legend > span')
    await this.page.waitForSelector('#co-payment-form > fieldset > div.shipping-information')
    const paymentPageUrl = await this.page.url()
    console.log('Payment Page URL:',paymentPageUrl)
    expect(paymentPageUrl).to.include('/checkout/#payment')//斷言:此page的url必須包含example.com
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.waitForSelector('#opc-sidebar > div.opc-block-summary')
    //取得price, shipping, tax的文字
    const paymentCartSubtotalTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span'
    await this.page.waitForSelector(paymentCartSubtotalTextElement)
    const paymentCartSubtotalText = await this.page.$eval(paymentCartSubtotalTextElement,element=>element.innerHTML)
    //const paymentCartSubtotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span')
    const paymentShippingTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span'
    await this.page.waitForSelector(paymentShippingTextElement)
    const paymentShippingText = await this.page.$eval(paymentShippingTextElement,element=>element.innerHTML)    
    //const paymentShippingText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span')
    const paymentTaxTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span'
    await this.page.waitForSelector(paymentTaxTextElement)
    const paymentTaxText = await this.page.$eval(paymentTaxTextElement,element=>element.innerHTML)    
    //const paymentTaxText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span')
    const paymentOrderTotalTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span'
    await this.page.waitForSelector(paymentOrderTotalTextElement)
    const paymentOrderTotalText = await this.page.$eval(paymentOrderTotalTextElement,element=>element.innerHTML)
    //const paymentOrderTotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span')
    //去除EUR 
    const paymentCartSubtotalNumIncludeComma = paymentCartSubtotalText.replace("EUR ","")
    const paymentShippingOnlyNumIncludeComma = paymentShippingText.replace("EUR ","")
    const paymentTaxOnlyNumIncludeComma = paymentTaxText.replace("EUR ","")
    const paymentOrderTotalNumIncludeComma = paymentOrderTotalText.replace("EUR ","")
    //去除逗號 
    function clear(str) { 
        str = str.replace(/,/g, "");//取消字串中出現的所有逗號 
        return str; 
    } 
    const paymentCartSubtotalNum = clear(paymentCartSubtotalNumIncludeComma)
    const paymentShippingOnlyNum = clear(paymentShippingOnlyNumIncludeComma)
    const paymentTaxOnlyNum = clear(paymentTaxOnlyNumIncludeComma)
    const paymentOrderTotalNum = clear(paymentOrderTotalNumIncludeComma)
    //將字串轉換成數字
    const paymentCartSubtotal = parseFloat(paymentCartSubtotalNum)
    const paymentShipping = parseFloat(paymentShippingOnlyNum)
    const paymentTax = parseFloat(paymentTaxOnlyNum )
    const paymentOrderTotal = parseFloat(paymentOrderTotalNum )
    console.log('cartSubtotal(Payment Page):',paymentCartSubtotal)
    console.log('shipping(Payment Page):',paymentShipping)
    console.log('tax(Payment Page):',paymentTax )
    console.log('orderTotal(Payment Page):',paymentOrderTotal)
    //確認total information上的金額
    const paymentOrderTotalActual = paymentCartSubtotal+paymentShipping+paymentTax
    const paymentTaxRate = 0.22 //義大利稅率
    const paymentCartSubtotalAndShipping = paymentCartSubtotal+paymentShipping
    const paymentTaxActualUnfixed = paymentCartSubtotalAndShipping*paymentTaxRate
    const paymentTaxActualFixed = paymentTaxActualUnfixed.toFixed(2)//四捨五入取到小數點第二位
    const paymentTaxActual= parseFloat(paymentTaxActualFixed)//因為toFixed(2)回傳的是字串
    console.log('totalActual(Payment Page):',paymentOrderTotalActual)//實際計算(應得)的數字
    console.log('taxActual(Payment Page):',paymentTaxActual)//實際計算(應得)的數字
    //確認total information和實際計算(應得)的數字是否相同
    expect(paymentOrderTotal).to.equal(paymentOrderTotalActual)
    expect(paymentTax).to.equal(paymentTaxActual)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})
Given("[EN-EU Member]Cart Page - Remove Item",{timeout: 24 * 5000},async function(){
    //移除購物車品項, 免得商品按到缺貨
    await this.page.goto('https://shop.benq.eu/eu-buy/checkout/cart/'+cicGA)
    await this.page.waitForSelector('#shopping-cart-table > tbody > tr > td.col.actionsiso > div > a.action.action-delete')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.click('#shopping-cart-table > tbody > tr > td.col.actionsiso > div > a.action.action-delete')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.waitForSelector('body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap')
    await this.page.waitForSelector('body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap > footer > button.action-primary.action-accept')
    await this.page.click('body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap > footer > button.action-primary.action-accept')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const removeButton = 'body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap > footer > button.action-primary.action-accept'
    await this.page.waitForSelector(removeButton,{
        hidden:true, 
        timeout:10000
    })//等待element隱藏的時間是否在10000毫秒內
    //await shouldNotExist(page,'body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap > footer > button.action-primary.action-accept')
    await this.page.waitForSelector('#maincontent > div.columns > div > div.cart-empty > p:nth-child(1)')
    const emptyCartTextElement = '#maincontent > div.columns > div > div.cart-empty > p:nth-child(1)'
    await this.page.waitForSelector(emptyCartTextElement)
    const emptyCart = await this.page.$eval(emptyCartTextElement,element=>element.innerHTML)
    //const emptyCart= await getText(page, '#maincontent > div.columns > div > div.cart-empty > p:nth-child(1)')
    expect(emptyCart).to.be.a('string','You have no items in your shopping cart.')
    await this.page.waitForTimeout(3000)//等待3000毫秒
})

Given("[ZH-TW Member]Go to Product Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.com/zh-tw/speaker/electrostatic-bluetooth-speaker/trevolo-s.html'+cicGA)
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const buyNow = '#Not-simpleFlow'
    await this.page.waitForSelector(buyNow)
    await this.page.click(buyNow)
    await this.page.waitForTimeout(20000)//等待20000毫秒
})
When("[ZH-TW Member]Go to EC Product Page",{timeout: 24 * 5000},async function(){
    await this.page.waitForSelector('#maincontent')
    const buyNow = '#product-addtocart-button'
    await this.page.waitForSelector(buyNow)
    await this.page.click(buyNow)
    await this.page.waitForTimeout(20000)//等待20000毫秒
})
When("[ZH-TW Member]Cart Page",{timeout: 36 * 5000},async function(){
    //Cart Page
    const cartPageUrl = await this.page.url()
    console.log('Cart Page URL:',cartPageUrl)
    expect(cartPageUrl).to.include('checkout/cart')//斷言:此page的url必須包含example.com
    
    await this.page.waitForTimeout(5000)
    //'#maincontent > div.columns > div > div.cart-summary > ul > li:nth-child(3) > button'
    const memberCheckoutButton = 'button.action.primary.checkout'
    await this.page.waitForSelector(memberCheckoutButton)//Guest Checkout
    await this.page.click(memberCheckoutButton)//Guest Checkout
    await this.page.waitForTimeout(20000)//等待20000毫秒
})
When("[ZH-TW Member]Login Page",{timeout: 12 * 5000},async function(){
    const loginPageUrl = await this.page.url()
    console.log('Login Page URL:',loginPageUrl)
    expect(loginPageUrl).to.include('https://club.benq.com/ICDS/Home/BenQAuth?system_id=G5&function=Login&lang=zh-tw')//斷言:此page的url必須包含example.com
    await this.page.waitForTimeout(1000)//等待1000毫秒
    await this.page.waitForSelector('body > div.block.log_in > div > div > div.col-sm-7.col-xs-12.log_left > div')
    await this.page.type('#userName','celinetest1234@gmail.com',{delay:100})
    await this.page.type('#password','test1234',{delay:100})
    await this.page.click('#login')
    await this.page.waitForTimeout(30000)//等待30000毫秒
})
When("[ZH-TW Member]Shipping Page",{timeout: 12 * 5000},async function(){
    const shippingPageUrl = await this.page.url()
    console.log('Shipping Page URL:',shippingPageUrl)
    expect(shippingPageUrl).to.include('/checkout/')//斷言:此page的url必須包含example.com
    await this.page.waitForSelector('#shipping')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const chooseAddress = '#checkout-step-shipping > div > div > div > div.shipping-address-item.not-selected-item > button.action.action-select-shipping-item'
    await this.page.waitForSelector(chooseAddress)
    await this.page.click(chooseAddress)//選擇地址簿
    await this.page.waitForSelector('#opc-sidebar > div.opc-block-summary')
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //取得price, shipping, tax的文字
    const shippingCartSubtotalTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span'
    await this.page.waitForSelector(shippingCartSubtotalTextElement)
    const shippingCartSubtotalText = await this.page.$eval(shippingCartSubtotalTextElement,element=>element.innerHTML)
    //const shippingCartSubtotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span')
    const shippingShippingTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span'
    await this.page.waitForSelector(shippingShippingTextElement)
    const shippingShippingText = await this.page.$eval(shippingShippingTextElement,element=>element.innerHTML)
    //const shippingShippingText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span')
    // const shippingTaxTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span'
    // await this.page.waitForSelector(shippingTaxTextElement)
    // const shippingTaxText = await this.page.$eval(shippingTaxTextElement,element=>element.innerHTML)
    //const shippingTaxText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span')
    const shippingOrderTotalElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span'
    await this.page.waitForSelector(shippingOrderTotalElement)
    const shippingOrderTotalText = await this.page.$eval(shippingOrderTotalElement,element=>element.innerHTML)
    //const shippingOrderTotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span')
    //去除EUR 
    const shippingCartSubtotalNumIncludeComma = shippingCartSubtotalText.replace("NT$","")
    const shippingShippingOnlyNumIncludeComma = shippingShippingText.replace("NT$","")
    // const shippingTaxOnlyNumIncludeComma = shippingTaxText.replace("EUR ","")
    const shippingOrderTotalNumIncludeComma = shippingOrderTotalText.replace("NT$","")
    //去除逗號 
    function clear(str) { 
        str = str.replace(/,/g, "");//取消字串中出現的所有逗號 
        return str; 
    } 
    const shippingCartSubtotalNum = clear(shippingCartSubtotalNumIncludeComma)
    const shippingShippingOnlyNum = clear(shippingShippingOnlyNumIncludeComma)
    //const shippingTaxOnlyNum = clear(shippingTaxOnlyNumIncludeComma)
    const shippingOrderTotalNum = clear(shippingOrderTotalNumIncludeComma)
    //將字串轉換成數字(包含小數值)
    const shippingCartSubtotal = parseFloat(shippingCartSubtotalNum)
    const shippingShipping = parseFloat(shippingShippingOnlyNum)
    //const shippingTax = parseFloat(shippingTaxOnlyNum)
    const shippingOrderTotal = parseFloat(shippingOrderTotalNum )
    console.log('cartSubtotal(Shipping Page):',shippingCartSubtotal)
    console.log('shipping(Shipping Page):',shippingShipping)
    //console.log('tax(Shipping Page):',shippingTax )
    console.log('orderTotal(Shipping Page):',shippingOrderTotal)
    //確認total information上的金額
    const shippingOrderTotalActual = shippingCartSubtotal+shippingShipping
    //const shippingTaxRate = 0 //台灣0稅率
    //const shippingCartSubtotalAndShipping = shippingCartSubtotal+shippingShipping
    //const shippingTaxActualUnfixed = shippingCartSubtotalAndShipping*shippingTaxRate
    //const shippingTaxActualFixed = shippingTaxActualUnfixed.toFixed(2)//四捨五入取到小數點第二位
    //const shippingTaxActual= parseFloat(shippingTaxActualFixed)//因為toFixed(2)回傳的是字串
    console.log('totalActual(Shipping Page):',shippingOrderTotalActual)//實際計算(應得)的數字
    //console.log('taxActual(Shipping Page):',shippingTaxActual)//實際計算(應得)的數字
    //確認total information和實際計算(應得)的數字是否相同
    expect(shippingOrderTotal).to.equal(shippingOrderTotalActual)
    //expect(shippingTax).to.equal(shippingTaxActual)
    await this.page.click('#next-step-trigger')//Next
    await this.page.waitForSelector('#next-step-trigger',{
        hidden:true, 
        timeout:10000
    })//等待element隱藏的時間是否在10000毫秒內
    //await shouldNotExist(page,'#next-step-trigger')//Next
})
Then("[ZH-TW Member]Payment Page",{timeout: 12 * 5000},async function(){
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.waitForSelector('#co-payment-form > fieldset > legend > span')
    await this.page.waitForSelector('#co-payment-form > fieldset > div.shipping-information')
    const paymentPageUrl = await this.page.url()
    console.log('Payment Page URL:',paymentPageUrl)
    expect(paymentPageUrl).to.include('/checkout/#payment')//斷言:此page的url必須包含example.com
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.waitForSelector('#opc-sidebar > div.opc-block-summary')
    //取得price, shipping, tax的文字
    const paymentCartSubtotalTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span'
    await this.page.waitForSelector(paymentCartSubtotalTextElement)
    const paymentCartSubtotalText = await this.page.$eval(paymentCartSubtotalTextElement,element=>element.innerHTML)
    //const paymentCartSubtotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.sub > td > span')
    const paymentShippingTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span'
    await this.page.waitForSelector(paymentShippingTextElement)
    const paymentShippingText = await this.page.$eval(paymentShippingTextElement,element=>element.innerHTML)    
    //const paymentShippingText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals.shipping.excl > td > span')
    //const paymentTaxTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span'
    //await this.page.waitForSelector(paymentTaxTextElement)
    //const paymentTaxText = await this.page.$eval(paymentTaxTextElement,element=>element.innerHTML)    
    //const paymentTaxText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.totals-tax > td > span')
    const paymentOrderTotalTextElement = '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span'
    await this.page.waitForSelector(paymentOrderTotalTextElement)
    const paymentOrderTotalText = await this.page.$eval(paymentOrderTotalTextElement,element=>element.innerHTML)
    //const paymentOrderTotalText = await getText(page, '#opc-sidebar > div.opc-block-summary > table > tbody > tr.grand.totals > td > strong > span')
    //去除EUR 
    const paymentCartSubtotalNumIncludeComma = paymentCartSubtotalText.replace("NT$","")
    const paymentShippingOnlyNumIncludeComma = paymentShippingText.replace("NT$","")
    //const paymentTaxOnlyNumIncludeComma = paymentTaxText.replace("EUR ","")
    const paymentOrderTotalNumIncludeComma = paymentOrderTotalText.replace("NT$","")
    //去除逗號 
    function clear(str) { 
        str = str.replace(/,/g, "");//取消字串中出現的所有逗號 
        return str; 
    } 
    const paymentCartSubtotalNum = clear(paymentCartSubtotalNumIncludeComma)
    const paymentShippingOnlyNum = clear(paymentShippingOnlyNumIncludeComma)
    //const paymentTaxOnlyNum = clear(paymentTaxOnlyNumIncludeComma)
    const paymentOrderTotalNum = clear(paymentOrderTotalNumIncludeComma)
    //將字串轉換成數字
    const paymentCartSubtotal = parseFloat(paymentCartSubtotalNum)
    const paymentShipping = parseFloat(paymentShippingOnlyNum)
    //const paymentTax = parseFloat(paymentTaxOnlyNum )
    const paymentOrderTotal = parseFloat(paymentOrderTotalNum )
    console.log('cartSubtotal(Payment Page):',paymentCartSubtotal)
    console.log('shipping(Payment Page):',paymentShipping)
    //console.log('tax(Payment Page):',paymentTax )
    console.log('orderTotal(Payment Page):',paymentOrderTotal)
    //確認total information上的金額
    const paymentOrderTotalActual = paymentCartSubtotal+paymentShipping
    //const paymentTaxRate = 0 //台灣0稅率
    // const paymentCartSubtotalAndShipping = paymentCartSubtotal+paymentShipping
    // const paymentTaxActualUnfixed = paymentCartSubtotalAndShipping*paymentTaxRate
    // const paymentTaxActualFixed = paymentTaxActualUnfixed.toFixed(2)//四捨五入取到小數點第二位
    // const paymentTaxActual= parseFloat(paymentTaxActualFixed)//因為toFixed(2)回傳的是字串
    console.log('totalActual(Payment Page):',paymentOrderTotalActual)//實際計算(應得)的數字
    //console.log('taxActual(Payment Page):',paymentTaxActual)//實際計算(應得)的數字
    //確認total information和實際計算(應得)的數字是否相同
    expect(paymentOrderTotal).to.equal(paymentOrderTotalActual)
    //expect(paymentTax).to.equal(paymentTaxActual)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})
Given("[ZH-TW Member]Cart Page - Remove Item",{timeout: 24 * 5000},async function(){
    //移除購物車品項, 免得商品按到缺貨
    await this.page.goto('https://store.benq.com/tw-b2c/checkout/cart/'+cicGA)
    await this.page.waitForSelector('#shopping-cart-table > tbody > tr > td.col.actionsiso > div > a.action.action-delete')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.click('#shopping-cart-table > tbody > tr > td.col.actionsiso > div > a.action.action-delete')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    await this.page.waitForSelector('body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap')
    await this.page.waitForSelector('body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap > footer > button.action-primary.action-accept')
    await this.page.click('body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap > footer > button.action-primary.action-accept')
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const removeButton = 'body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap > footer > button.action-primary.action-accept'
    await this.page.waitForSelector(removeButton,{
        hidden:true, 
        timeout:10000
    })//等待element隱藏的時間是否在10000毫秒內
    //await shouldNotExist(page,'body > div.modals-wrapper > aside.modal-popup.confirm._show > div.modal-inner-wrap > footer > button.action-primary.action-accept')
    await this.page.waitForSelector('#maincontent > div.columns > div > div.cart-empty > p:nth-child(1)')
    const emptyCartTextElement = '#maincontent > div.columns > div > div.cart-empty > p:nth-child(1)'
    await this.page.waitForSelector(emptyCartTextElement)
    const emptyCart = await this.page.$eval(emptyCartTextElement,element=>element.innerHTML)
    //const emptyCart= await getText(page, '#maincontent > div.columns > div > div.cart-empty > p:nth-child(1)')
    expect(emptyCart).to.be.a('string','您的購物車內沒有商品。')
    await this.page.waitForTimeout(3000)//等待3000毫秒
})
