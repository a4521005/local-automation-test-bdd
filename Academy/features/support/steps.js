const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const puppeteer = require('puppeteer')

Before({timeout: 24 * 5000},async function () {
    
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

Given("Open the browser",{timeout: 12 * 5000},async function(){
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

When("Go to Register Page",{timeout: 12 * 5000},async function(){
})

When("Fill out the register form (profession:ITS)",{timeout: 24 * 5000},async function(){
})

Then("create a new account successfully (profession:ITS)",{timeout: 12 * 5000},async function(){
})

When("Fill out the register form (profession:Reseller)",{timeout: 24 * 5000},async function(){
})

Then("create a new account successfully (profession:Reseller)",{timeout: 12 * 5000},async function(){
})

When("Fill out the register form (profession:Teacher)",{timeout: 24 * 5000},async function(){
})

Then("create a new account successfully (profession:Teacher)",{timeout: 12 * 5000},async function(){
})