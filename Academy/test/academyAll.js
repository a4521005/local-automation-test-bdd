const puppeteer = require('puppeteer');
const expect = require('chai').expect;
const fs = require('fs');
const {click,getText,getCount,shouldExist,waitForText} = require('../lib/helper')

const generateEmailonAcademy = require('../lib/utils').generateEmailonAcademy

//一次創三個帳號, 職業分別是ITS, Reseller, Teacher
//每個帳號分別去參加所有課程+考試+取得所有課程證書
describe('Take all training classes (profession:ITS)',()=>{
    let browser
    let page
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            headless:false,//有無需要開視窗,false要開,true不開
            slowMo:110,// slow down by 110ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        await page.setViewport({width:1200,height:1000})
        await page.setDefaultTimeout(10000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(20000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    after(async function(){
        await browser.close()
    })
    it('create a new account successfully (profession:ITS)',async function(){
        await page.goto('https://www.benq.academy/register')
        await page.waitForSelector('form')
        await page.waitForSelector('#register')
        //First name
        await page.type('form > div:nth-child(1) > div:nth-child(1) > input ','Celine',{delay:10})
        await page.waitForTimeout(100)//等待100毫秒
        //Last Name
        await page.type('form > div:nth-child(1) > div:nth-child(2) > input ','Test ',{delay:10})
        await page.waitForTimeout(100)//等待100毫秒
        //E-mail
        const testEmail = generateEmailonAcademy()
        await page.type('form > div:nth-child(2) > input ',testEmail,{delay:10})
        console.log("test email:",testEmail)
        await page.waitForTimeout(5000)
        //Company, school or organization name
        await page.type('#Workplace','Test By Celine',{delay:10})
        await page.waitForTimeout(100)//等待100毫秒
        //Profession(Teacher/Reseller/ITS)
        //選擇ITS
        await page.select('form > div:nth-child(4) > select', '3')
        // await selectITS.type('ITS');
        //Password
        await page.type('form > div:nth-child(5)  > input ','test123',{delay:10})
        await page.waitForTimeout(100)//等待100毫秒
        //Re-enter Password
        await page.type('form > div:nth-child(6) > input ','test123',{delay:10})
        await page.waitForTimeout(100)//等待100毫秒
        //點擊privace policy
        await page.click('form > div.text-xs > label > input',{clickCount:1})
        await page.waitForTimeout(5000)//等待5000毫秒
        //點擊submit
        await page.click('form > div.flex.justify-center > button')
        await page.waitForTimeout(10000)//等待10000毫秒
        //註冊完會自動導向至首頁
        const afterSignUpurl = await page.url()
        expect(afterSignUpurl).to.include('https://www.benq.academy/')//斷言:此page的url必須包含https://www.benq.academy/
        console.log("After Sign Up URL(ITS) :",afterSignUpurl)
        //Save Account name to the text file
        const date = new Date()
        function wholeMonth(){
            var getmonth = date.getMonth() + 1
            if(getmonth<10){
                wholeMonth =  "0"+getmonth 
                return wholeMonth
            }else{
                wholeMonth = getmonth 
                return wholeMonth
            }
        }
        function wholeDate(){
            const getDay = date.getDate()
            if(getDay<10){
                wholeDate =  "0"+getDay
                return wholeDate
            }else{
                wholeDate = getDay 
                return wholeDate
            }
        }
        const month = wholeMonth()
        const day = wholeDate()
        const year = date.getFullYear()
        const fullDate = `${year}${month}${day}`
        const time = ' '+date.getHours()+':'+date.getMinutes();
        const fullTime = `${year}${month}${day}${time}`
        const fileName = "./logForAccount/logForAccount-"
        const todayDate = fullDate
        const fileTxt = ".txt"
        const wholeFileName = fileName+todayDate+fileTxt
        const logger = fs.createWriteStream(wholeFileName, {flags: 'a'})
        const professionName = "ITS(profession)"
        const className = "AWS(class)"
        logger.write(`${fullTime} - ${professionName} - ${className} - ${testEmail}\n`)
        logger.close()
    })
    it('[ITS]Count all the classes',async function(){
        await page.click('ul.nav-menu > li:nth-child(2) > a')
        await page.waitForTimeout(10000)//等待10000毫秒
        const trainedurl = await page.url()
        expect(trainedurl).to.include('/trained')//斷言:此page的url必須包含/trained
        await page.waitForTimeout(5000)//等待5000毫秒
        //等待課程列表出現
        await page.waitForSelector('#app')
    })
    it('Get count-all training classes for ITS(15 classes)',async function(){
        const itsTrainingClassList='#app > div > div:nth-child(2) > ul > li'
        const countItsTrainingClassList = await getCount(page, itsTrainingClassList)
        console.log("Total of Training Classes(ITS):",countItsTrainingClassList)
        expect(countItsTrainingClassList).to.equal(15)//目前GamingProjector有8個產品
    })
    
    it('[ITS/AMS]Take a training class-AMS',async function(){
        //前往Trained Page
        await page.click('ul.nav-menu > li:nth-child(2) > a')
    })
    it('[ITS/AMS]Take a Certification-AMS',async function(){
    })
    it('[ITS/AMS]Pass the test-AMS',async function(){
    })
    it('[ITS/AMS]Fill out the form',async function(){
    })
    it('[ITS/AMS]Get my Certification',async function(){
    })
})

// describe('[ITS]Take a training class and get a certification - AMS',()=>{
//     it('[ITS/AMS]Take a training class',async function(){
//     })
//     it('[ITS/AMS]Take a quiz(Certification)',async function(){
//     })
//     it('[ITS/AMS]pass the test',async function(){
//     })
//     it('[ITS/AMS]Fill out the form - input first name & last name & company',async function(){
//     })
//     it('[ITS/AMS]Get my Certification',async function(){
//     })
// })

// describe('Log Out Successfully',()=>{
//     let browser
//     // it('Click Log Out Button',async function(){
//     // })
//     it('Log Out Successfully and close the browser',async function(){
//         //close the browser
//         await browser.close()
//     })
// })
    
    



